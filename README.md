#Running app
To run this application on your machine you need to have Docker installed!

After you make sure that Docker is present run following command:  
`docker-compose up` 

Once everything is up and running open in your favorite web browser following URL:  
`http://localhost:3000`
